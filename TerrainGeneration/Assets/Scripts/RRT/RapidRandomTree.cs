﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct Line
{
	public Vector2 begin, end;
};

[System.Serializable]
public class RapidRandomTree
{
	public RANDOM_TYPE sampleType;
	public UniqueRandomNumberGenerator randX, randY;
	public Vector2 initialPoint;
	public List<Line> lines;
	public int iterations;
	public float growthRate;
	public bool liveIteration = false, finished = false;
	
	private List<Vector2> points;
	private float closestDistance;
	private float growthSquared;
	
	public RapidRandomTree() //default constructor, sets up default values in case some values do not need to be set specifically.
	{
		initialPoint = Vector2.zero;
		iterations = 100;
		growthRate = 5;
		growthSquared = growthRate * growthRate;
		sampleType = RANDOM_TYPE.UNIQUE_POINTS;
		
		randX = new UniqueRandomNumberGenerator();
		randY = new UniqueRandomNumberGenerator();
	}
	
	public void setTreeProperties(int iterations, float growth, RANDOM_TYPE type, bool live)
	{
		this.iterations = iterations;
		this.growthRate = growth;
		this.sampleType = type;
		this.liveIteration = live;
		
		growthSquared = growth * growth;
	}
	
	public void updateTree()
	{
		if(!finished)
		{
			if(liveIteration)
				exploreTreeLive();
			else
				exploreTree();
		}
	}
	
	public void initialise()
	{
		points = new List<Vector2>(iterations);
		lines = new List<Line>(iterations);
		randX.setNumberRange(iterations);
		randY.setNumberRange(iterations);
		switch(sampleType)
		{
			case RANDOM_TYPE.CHAOTIC_POINTS:
			initialPoint.x = Random.Range(0, iterations);
			initialPoint.y = Random.Range(0, iterations);
			break;
			
			case RANDOM_TYPE.UNIQUE_POINTS:
			initialPoint = samplePosition();
			break;
		}
		points.Add(initialPoint);
		growthSquared = growthRate * growthRate;
	}
	
	public List<Vector2> returnPoints()
	{
		return points;
	}
	
	private void exploreTreeLive()
	{
		if(iterations > 0)
		{
			Vector2 sample = samplePosition();
			Vector2 neighbour = findClosestPoint(sample);
			sample = validateGrowthSize(sample, neighbour);
			Line newLine = new Line();
			newLine.begin = neighbour;
			newLine.end = sample;
			lines.Add(newLine);
			points.Add(sample);
			iterations--;
		}
		else
		{
			finished = true;
		}
	}
	
	private void exploreTree()
	{
		for(int i = 0; i < iterations; ++i)
		{
			Vector2 sample = samplePosition();
			Vector2 neighbour = findClosestPoint(sample);
			sample = validateGrowthSize(sample, neighbour);
			
			Line newLine = new Line();
			newLine.begin = neighbour;
			newLine.end = sample;
			lines.Add(newLine);
			points.Add(sample);
		}
		
		finished = true;
	}
	
	private Vector2 validateGrowthSize(Vector2 sample, Vector2 neighbour)
	{
		Vector2 pointDifference = sample - neighbour;
		
		if(pointDifference.sqrMagnitude > growthSquared)
		{
			pointDifference.Normalize();
			pointDifference *= growthRate;
			
			return neighbour + pointDifference;
		}
		
		return sample;
	}
	
	private Vector2 findClosestPoint(Vector2 sample)
	{
		Vector2 point = Vector2.zero;
		closestDistance = float.MaxValue;
		float currDist;
		for(int i = 0; i < points.Count; ++i)
		{
			Vector2 currPoint = points[i];
			Vector2 pointDifference = currPoint - sample;
			currDist = Vector2.SqrMagnitude(pointDifference);
			if(currDist < closestDistance)
			{
				closestDistance = currDist;
				point = currPoint;
			}
		}
		
		closestDistance = float.MaxValue;
		
		return point;
	}
	
	private Vector2 samplePosition()
	{
		Vector2 sample = Vector2.zero;
		switch(sampleType)
		{
			case RANDOM_TYPE.CHAOTIC_POINTS:
			sample.x = getRandomX();
			sample.y = getRandomY();
			break;
			
			case RANDOM_TYPE.UNIQUE_POINTS:
			sample.x = getUniqueRandomX();
			sample.y = getUniqueRandomY();
			break;
		}
		
		return sample;
	}
	
	private int getRandomX()
	{
		return (int)(Random.Range(0, iterations));
	}
	
	private int getRandomY()
	{
		return (int)(Random.Range(0, iterations));
	}
	
	private int getUniqueRandomX()
	{
		return randX.getNewRandomNum();
	}
	
	private int getUniqueRandomY()
	{
		return randY.getNewRandomNum();
	}
}

public enum RANDOM_TYPE
{
	UNIQUE_POINTS,
	CHAOTIC_POINTS
};
