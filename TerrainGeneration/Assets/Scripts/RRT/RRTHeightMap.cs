﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class RRTHeightMap : MonoBehaviour {

	public bool debugPoints = false;
	public float heightOffset;
	public RRTLineRenderer rrtRenderer;
	public PlaneGenerator planeMaker;
	private RapidRandomTree rrt;
	private int rowCount;
	private List<Vector2> points;
	
	void Awake()
	{
		rrtRenderer.iterations = planeMaker.resolution;
		rowCount = planeMaker.resolution;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	private void drawPoints()
	{
		for(int i = 0; i < points.Count; ++i)
		{
			GameObject.CreatePrimitive(PrimitiveType.Cube).transform.position = points[i];
		}
	}
	
	// Update is called once per frame
	void Update () {
		findAllPointsInLines();
//		debugMeshVerts();
		if(debugPoints)
		{
			drawPoints();
		}
		else
			applyPointsToPlane();
		this.enabled = false;
	}
	
	private void debugMeshVerts()
	{
		MeshFilter filter = planeMaker.gameObject.GetComponent<MeshFilter>();
		Vector3[] verts = filter.mesh.vertices;
		
		for(int i = 0; i < (rowCount+1); ++i) //a single row of verts is rowCount+1
		{
			print ("Modifying vert: " + i);
			verts[i].y = 1;
			print (verts[i]);
		}
		
		filter.mesh.vertices = verts;
	}
	
	private void findAllPointsInLines()
	{
		rrt = rrtRenderer.rrt;
		points = new List<Vector2>(rrt.lines.Count);
		Line[] lines = rrt.lines.ToArray();
		
		for(int i = 0; i < lines.Length; ++i)
		{
			//supply a line and find all points inside of it
			findPointsOnLine(lines[i]);
		}
	}
	
	private void findPointsOnLine(Line line)
	{
		//something fucked up in the point distribution, suggest rendering the points found as lines to check if pattern is right or array stepping is wrong
		Vector2 lineSize;
		Vector2 normalisedSize;
		Vector2 currentPointStep;
		Vector2 newPoint = line.end + line.begin;
		lineSize = line.end - line.begin;
		normalisedSize = lineSize.normalized;
		int growthRate = (int)rrt.growthRate;
		for(int i = 0; i < growthRate; ++i)
		{
			currentPointStep = normalisedSize * i; //find the point step
			newPoint = line.begin + currentPointStep; //get the new point on the line
			
			newPoint.x = Mathf.Round(newPoint.x); //round the values to be used for indexing
			newPoint.y = Mathf.Round(newPoint.y); //round the values to be used for indexing
//			newPoint.x = (int)newPoint.x;
//			newPoint.y = (int)newPoint.y;
			
			points.Add(newPoint);
		}
	}
	
	private void applyPointsToPlane()
	{
		rrt = rrtRenderer.rrt;
		int pointCount = points.Count;
		
		MeshFilter filter = planeMaker.gameObject.GetComponent<MeshFilter>();
		Vector3[] verts = filter.mesh.vertices;
		
		for(int i = 0; i < pointCount; ++i)
		{
			int xIndex = (int)points[i].x;
			int yIndex = (int)points[i].y;
			int currentVertIndex = (rowCount+1) * yIndex + xIndex;
			
			try
			{
				verts[currentVertIndex].y = heightOffset;
//				print ("Current vert: " + currentVertIndex + "(" + verts[currentVertIndex] + ")");
			} catch(Exception e)
			{
				print ("Exception message: " + e.Message);
				print ("Out of bounds: " + currentVertIndex);
				print ("RowCount: " + rowCount + " xIndex: " + xIndex + " yIndex: " + yIndex);
			}
		}
		
		filter.mesh.vertices = verts;
		filter.mesh.RecalculateNormals();
		
//		for(int z = 0; z < rowCount; ++z)
//		{
//			for(int x = 0; x < columnCount; ++x)
//			{
//				int currentVertIndex = rowCount * z + x; //Row amount x current row + current column
//				
//			}
//		}
	}
}
