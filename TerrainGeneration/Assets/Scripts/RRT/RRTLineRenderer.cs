﻿using UnityEngine;
using System.Collections;

public class RRTLineRenderer : MonoBehaviour {

	[HideInInspector]public RapidRandomTree rrt;
	public RANDOM_TYPE sampleBehaviour;
	public bool liveRendering = false;
	public int iterations;
	public float growthRate;
	public LineRenderer linePrefab;

	void Awake()
	{
		
	}

	private void initialiseRRT()
	{
		rrt = new RapidRandomTree();
		rrt.setTreeProperties(iterations, growthRate, sampleBehaviour, liveRendering);
		rrt.initialise();
	}

	// Use this for initialization
	void Start () {
		initialiseRRT();
		if(!liveRendering)
		{
			rrt.updateTree();
			//draw here by getting line list
			drawLines(liveRendering);
		}
	}
	
	private void drawLines(bool iterative)
	{
		if(!iterative)
		{
			Line[] lines = rrt.lines.ToArray();
			for(int i = 0; i < lines.Length; ++i)
			{
				LineRenderer line = Instantiate(linePrefab) as LineRenderer;
				line.SetVertexCount(2);
				line.SetPosition(0, lines[i].begin);
				line.SetPosition(1, lines[i].end);
				line.transform.parent = transform;
			}
		}
		else
		{
			LineRenderer line = Instantiate(linePrefab) as LineRenderer;
			line.SetVertexCount(2);
			int currI = rrt.lines.Count - 1;
			line.SetPosition(0, rrt.lines[currI].begin);
			line.SetPosition(1, rrt.lines[currI].end);
			line.transform.parent = transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(liveRendering)
		{
			if(!rrt.finished)
			{
				rrt.updateTree();
				//draw most recent line by getting line list and using count - 1
				drawLines(liveRendering);
			}
		}
	}
}
