﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class UniqueRandomNumberGenerator
{
	public int range;
	private int max;
	private int[] availableNumbers;
	
	public UniqueRandomNumberGenerator()
	{
		range = 1;
		initialiseGenerator();
	}
	
	public void setNumberRange(int range)
	{
		this.range = range;
		initialiseGenerator();
	}
	
	private void initialiseGenerator()
	{
		if(range < 1)
		{
			Debug.LogWarning(this + ": Range needs to be 1 or greater");
		}
		availableNumbers = new int[range];
		resetMaxNumbers();
	}
	
	public int getNewRandomNum() //Makes sure a unique number is randomised without using a loop. Google Fisher-Yates shuffle
	{
		int i = Random.Range(0, max);
		int tempR = 0;
		int tempM = 0;
		int randomNumber = availableNumbers[i];
		
		tempR = availableNumbers[i];
		tempM = availableNumbers[max];
		
		availableNumbers[i] = tempM;
		availableNumbers[max] = tempR;
		
		--max;
		
		if(max < 0)
		{
			resetMaxNumbers();
		}
		
		return randomNumber;
	}
	
	private void resetMaxNumbers()
	{
		for(int i = 0; i < availableNumbers.Length; ++i)
		{
			availableNumbers[i] = i; //reset the values from 0 - max
		}
		
		max = range - 1;
        if(max < 0)
        {
            max = 0;
        }
	}
}
