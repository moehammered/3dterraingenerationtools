﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlaneMeshGenerator
{
	public int width, floorHeight;
	public int horizontalSegments, verticalSegments;
	public Mesh currentlyGeneratedMesh;
	private int quadCount, triangleCount, vertexCount;
	private Vector3[] vertices;
	private Vector2[] uvs;
	private int[] triangleIndices;
	
	private const int uShortLimit = 65535; //Upper limit for vertices on a single mesh
	
	public PlaneMeshGenerator()
	{
		width = 20;
		floorHeight = 0;
		horizontalSegments = 12;
		verticalSegments = 12;
		quadCount = triangleCount = vertexCount = 0;
	}
	
	public PlaneMeshGenerator(int width, int floorHeight)
	{
		this.width = width;
		this.floorHeight = floorHeight;
		horizontalSegments = 12;
		verticalSegments = 12;
		quadCount = triangleCount = vertexCount = 0;
		
		calculateMeshInformation();
	}
	
	public PlaneMeshGenerator(int width, int floorHeight, int xSegments, int zSegments)
	{
		this.width = width;
		this.floorHeight = floorHeight;
		horizontalSegments = xSegments;
		verticalSegments = zSegments;
		quadCount = triangleCount = vertexCount = 0;
		
		calculateMeshInformation();
	}
	
	public void createMeshData()
	{
		calculateMeshInformation();
		createVertices();
		createTrianglesPerRow ();
	}
	
	public Mesh createMesh()
	{
		if(currentlyGeneratedMesh == null)
			currentlyGeneratedMesh = new Mesh();
			
		currentlyGeneratedMesh.name = "GeneratedMesh";
		currentlyGeneratedMesh.Clear();
		currentlyGeneratedMesh.vertices = vertices;
		currentlyGeneratedMesh.uv = uvs;
		currentlyGeneratedMesh.triangles = triangleIndices;
		currentlyGeneratedMesh.RecalculateNormals();

		return currentlyGeneratedMesh;
	}
	
	public void setSegments(int xSegments, int zSegments)
	{
		horizontalSegments = xSegments;
		verticalSegments = zSegments;
	}
	
	public void setDimensions(int width, int floorHeight)
	{
		this.width = width;
		this.floorHeight = floorHeight;
	}
	
	public void setVertexData(Vector3[] newVerts)
	{
		vertices = null;
		vertices = newVerts;
	}
	
	public Vector3[] getVertexData()
	{
		return vertices;
	}
	
	public int getVertexCount()
	{
		return vertexCount;
	}
	
	private void calculateMeshInformation()
	{
		calculateQuadCount();
		calculateTriangleCount();
		calculateVertexCount();
	}
	
	private void calculateQuadCount()
	{
		quadCount = horizontalSegments * verticalSegments;
	}
	
	private void calculateTriangleCount()
	{
		if(quadCount == 0)
			calculateQuadCount();
		
		triangleCount = quadCount * 2;
		
		triangleIndices = new int[triangleCount*3];
	}
	
	private void calculateVertexCount()
	{
		int verticesPerLine = horizontalSegments + 1;
		int verticalLineCount = verticalSegments + 1;
		vertexCount = verticesPerLine * verticalLineCount;
		if(vertexCount > uShortLimit) //uShortLimit is the max amount of verts unity accepts for a single mesh
			Debug.LogError(this + ": TOO MANY VERTICES! Reduce segments until vertex count is less than " + uShortLimit);
	}
	
	private void createVertices()
	{
		vertices = new Vector3[vertexCount];
		uvs = new Vector2[vertexCount];
		
		float segmentWidth, segmentHeight;
		
		segmentWidth = (float)width /(float)horizontalSegments;
		segmentHeight = (float)width /(float)verticalSegments;
		
		int x = 0, y = 0;
		
		for(int i = 0; i < vertices.Length; ++i)
		{
			float xPosition, zPosition;
			xPosition = x * segmentWidth;
			zPosition = y * segmentHeight;
			
			uvs[i] = new Vector2(xPosition/(float)width, zPosition/(float)width);
			vertices[i] = new Vector3(xPosition, floorHeight, zPosition);
			
			++x;
			if(x > horizontalSegments)
			{
				x = 0;
				++y;
				if(y > verticalSegments)
				{
					break; //reached end - shouldn't reach here since vertices.Length should be met
				}
			}
		}
	}
	
	private void debugVertexLocations(Vector3[] points)
	{
		for(int i = 0; i < points.Length; ++i)
		{
			GameObject.CreatePrimitive(PrimitiveType.Sphere).transform.position = points[i];
		}
	}
	
	private void createTrianglesPerRow()
	{
		//Set RLP, RUP, CWS, CHS - Refer to notes for further explanation of this functions behaviour
		int RLP = 0, RUP = 0; //RLP = Row Lower Point, RUP = Row Upper Point
		int CWS = 0, CHS = 0; //CWS = CurrentWidthSegment, CHS = CurrentHeightSegment
		
		RUP = horizontalSegments + 1;
		
		//Start
		for(int i = 0; i < triangleIndices.Length; i += 6)
		{
			createQuad(RLP, RUP, i);
			++RLP; //Move across on the bottom line
			++RUP; //Move across on the top line
			++CWS; //Currently on next quad segment
			if(CWS == horizontalSegments)
			{
				CWS = 0; //Back at the first x segment
				++CHS; //Move up a segment on the y/z
				++RLP; //Move across from the furthest right to reach back to left side (overflow from right side of vertices to left)
				++RUP; //Move across from the furthest right to reach back to left side (overflow from right side of vertices to left)
				if(CHS == verticalSegments) //Reached the very last line segment at the end of the mesh?
				{
					break;
				}
			}
		}
	}
	
	private void createQuad(int rlp, int rup, int triIndex)
	{
		//Triangle 1
		triangleIndices[triIndex] = rup;
		triangleIndices[triIndex+1] = rlp + 1;
		triangleIndices[triIndex+2] = rlp;
		
		//Triangle 2
		triangleIndices[triIndex+3] = rup;
		triangleIndices[triIndex+4] = rup+1;
		triangleIndices[triIndex+5] = rlp+1;
	}
}
