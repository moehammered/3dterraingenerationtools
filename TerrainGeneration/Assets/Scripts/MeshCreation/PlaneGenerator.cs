﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class PlaneGenerator : MonoBehaviour {
	
	protected PlaneMeshGenerator generator;
	[Range(1, 254)]
	public int resolution;
	public int size;

    protected MeshFilter currFilter;

	protected void initialiseGenerator()
	{
		generator = new PlaneMeshGenerator();
		generator.setSegments(resolution, resolution);
		generator.setDimensions(size, 0);
        generator.createMeshData();
        currFilter = GetComponent<MeshFilter>();
	}
	
    protected void createPlane()
    {
        Destroy(currFilter.mesh);
        currFilter.mesh = generator.createMesh();
    }

	protected void Awake()
	{
        initialiseGenerator();
        createPlane();
	}
	
	void Start()
	{
	}
	
	void Update()
	{
	}
}






