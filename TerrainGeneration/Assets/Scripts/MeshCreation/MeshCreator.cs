﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class MeshCreator : MonoBehaviour {

	public Vector3[] vertices;
	public Vector2[] uvs;
	public int[] triangleIndices;

	// Use this for initialization
	void Start () {
		GetComponent<MeshFilter>().mesh = constructMesh();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public Mesh constructMesh()
	{
		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.uv = uvs;
		mesh.triangles = triangleIndices;
		mesh.RecalculateNormals();
		return mesh;
	}
}
