﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DiamondSquareAlgorithm
{
	public int resolution;
	private float[] cornerValues;
	public float[,] heights;
	private float heightOffset = 8;
	
	/// <summary>
	/// Constants for consistent temporary array referral when dealing with point locations
	/// See performDiamonStep and performSquareStep function for example.
	/// </summary>
	private const int TOP_LEFT = 0, TOP_RIGHT = 1, BOTTOM_LEFT = 2, BOTTOM_RIGHT = 3, MID = 4,
						LEFT = 0, TOP = 1, RIGHT = 2, BOTTOM = 3;
	
	public DiamondSquareAlgorithm()
	{
		performInitialSetup(17, 1);
	}
	
	public DiamondSquareAlgorithm(int res)
	{
		performInitialSetup(res, 1);
	}

	public DiamondSquareAlgorithm(int res, float cornerHeight)
	{
		performInitialSetup(res, cornerHeight);
	}
	
	public void initialiseDisplacement()
	{
		int iteration = 1;
		
		Quadrant wholePlane = new Quadrant();
		wholePlane.tL = new GridPoint(0, resolution-1);
		wholePlane.tR = new GridPoint(resolution-1, resolution-1);
		wholePlane.bL = new GridPoint(0, 0);
		wholePlane.bR = new GridPoint(resolution-1, 0);
		wholePlane.mid = findMidPoint(wholePlane);
		
		Debug.Log("TLeftMain: " + wholePlane.tL);
		Debug.Log("TRightMain: " + wholePlane.tR);
		Debug.Log("BLeftMain: " + wholePlane.bL);
		Debug.Log("BRightMain: " + wholePlane.bR);
		Debug.Log("Mid: " + wholePlane.mid);
		
		performDiamondStep(wholePlane, iteration);
		performSquareStep(wholePlane, iteration);
		iteration++;
		
		calculateDisplacement(wholePlane, iteration);
	}
	
	private void calculateDisplacement(Quadrant parent, int iteration)
	{
		Quadrant[] subSquares = getSubSquares(parent);
		
		if(subSquares != null)
		{
			//perform diamond step on all current sub squares
			performDiamondStep(subSquares[TOP_LEFT], iteration);
			performDiamondStep(subSquares[TOP_RIGHT], iteration);
			performDiamondStep(subSquares[BOTTOM_LEFT], iteration);
			performDiamondStep(subSquares[BOTTOM_RIGHT], iteration);
			//perform square step on all current sub squares
			performSquareStep(subSquares[TOP_LEFT], iteration);
			performSquareStep(subSquares[TOP_RIGHT], iteration);
			performSquareStep(subSquares[BOTTOM_LEFT], iteration);
			performSquareStep(subSquares[BOTTOM_RIGHT], iteration);
			//iterate into next level of sub squares
			calculateDisplacement(subSquares[TOP_LEFT], iteration+1);
			calculateDisplacement(subSquares[TOP_RIGHT], iteration+1);
			calculateDisplacement(subSquares[BOTTOM_LEFT], iteration+1);
			calculateDisplacement(subSquares[BOTTOM_RIGHT], iteration+1);
		}
	}
	
	private Quadrant[] getSubSquares(Quadrant parent)
	{
		Quadrant[] quads = null;
		
		if(parent.mid.x - parent.tL.x > 1) //can we segment further?
		{
			quads = new Quadrant[4];
			quads[TOP_LEFT] = getTopLeftQuadrant(parent);
			quads[TOP_LEFT].mid = findMidPoint(quads[TOP_LEFT]);
			
			quads[TOP_RIGHT] = getTopRightQuadrant(parent);
			quads[TOP_RIGHT].mid = findMidPoint(quads[TOP_RIGHT]);
			
			quads[BOTTOM_LEFT] = getBottomLeftQuadrant(parent);
			quads[BOTTOM_LEFT].mid = findMidPoint(quads[BOTTOM_LEFT]);
			
			quads[BOTTOM_RIGHT] = getBottomRightQuadrant(parent);
			quads[BOTTOM_RIGHT].mid = findMidPoint(quads[BOTTOM_RIGHT]);
		}
		
		return quads;
	}
	
	/// <summary>
	/// Performs the square step. This is calculating the height value of the mid points on each edge.
	/// </summary>
	/// <param name="quad">Quad.</param>
	/// <param name="level">Level.</param>
	private void performSquareStep(Quadrant quad, int level)
	{
		float[] edges = new float[4];
		float[] outer = new float[5];
		
		//get outer points + midpoint
		outer[TOP_LEFT] = heights[quad.tL.x, quad.tL.z];
		outer[TOP_RIGHT] = heights[quad.tR.x, quad.tR.z];
		outer[BOTTOM_LEFT] = heights[quad.tL.x, quad.tL.z];
		outer[BOTTOM_RIGHT] = heights[quad.tL.x, quad.tL.z];
		outer[MID] = heights[quad.tL.x, quad.tL.z];
		
		//get edge mid points
		edges[LEFT] = heights[quad.returnLeftMid().x, quad.returnLeftMid().z];
		edges[RIGHT] = heights[quad.returnRightMid().x, quad.returnRightMid().z];
		edges[TOP] = heights[quad.returnTopMid().x, quad.returnTopMid().z];
		edges[BOTTOM] = heights[quad.returnBotMid().x, quad.returnBotMid().z];
		
		//calculate the points
		calculateLeftMid(ref edges[LEFT], outer[TOP_LEFT], outer[BOTTOM_LEFT], outer[MID], level);
		calculateRightMid(ref edges[RIGHT], outer[TOP_RIGHT], outer[BOTTOM_RIGHT], outer[MID], level);
		calculateTopMid(ref edges[TOP], outer[TOP_RIGHT], outer[TOP_LEFT], outer[MID], level);
		calculateBottomMid(ref edges[BOTTOM], outer[BOTTOM_RIGHT], outer[BOTTOM_LEFT], outer[MID], level);
		
		//set edge mid points
		heights[quad.returnLeftMid().x, quad.returnLeftMid().z] = edges[LEFT];
		heights[quad.returnRightMid().x, quad.returnRightMid().z] = edges[RIGHT];
		heights[quad.returnTopMid().x, quad.returnTopMid().z] = edges[TOP];
		heights[quad.returnBotMid().x, quad.returnBotMid().z] = edges[BOTTOM];
	}
	
	private void calculateLeftMid(ref float point, float t, float b, float r, int level)
	{
		if(Mathf.Abs(point) > 0)
			return;
			
		point = (r + t + b)/3f + (Random.Range(0f, heightOffset)/(float)level);
	}
	
	private void calculateRightMid(ref float point, float t, float b, float l, int level)
	{
		if(Mathf.Abs(point) > 0)
			return;
		
		point = (l + t + b)/3f + (Random.Range(0f, heightOffset)/(float)level);
	}
	
	private void calculateTopMid(ref float point, float r, float l, float b, int level)
	{
		if(Mathf.Abs(point) > 0)
			return;
		
		point = (l + r + b)/3f + (Random.Range(0f, heightOffset)/(float)level);
	}
	
	private void calculateBottomMid(ref float point, float r, float l, float t, int level)
	{
		if(Mathf.Abs(point) > 0)
			return;
		
		point = (l + r + t)/3f + (Random.Range(0f, heightOffset)/(float)level);
	}
	
	/// <summary>
	/// Performs the diamond step. This is calculating the midpoint height value of 4 corners.
	/// </summary>
	/// <param name="quad">Quad.</param>
	private void performDiamondStep(Quadrant quad, int level)
	{
		//0 = top left, 1 = top right, 2 = bottom left, 3 = bottom right, 4 = mid
		float[] points = new float[5];
		points[TOP_LEFT] = heights[quad.tL.x, quad.tL.z];
		points[TOP_RIGHT] = heights[quad.tR.x, quad.tR.z];
		points[BOTTOM_LEFT] = heights[quad.bL.x, quad.bL.z];
		points[BOTTOM_RIGHT] = heights[quad.bR.x, quad.bR.z];
		
		points[MID] = points[TOP_LEFT] + points[TOP_RIGHT] + points[BOTTOM_LEFT] + points[BOTTOM_RIGHT];
		points[MID] /= 4f;
		
		points[MID] += (Random.Range(0, heightOffset) / level);
		
		heights[quad.mid.x, quad.mid.z] = points[MID];
	}
	
	private void performInitialSetup(int res, float cornerHeight)
	{
		resolution = res;
		initialiseHeightsArray();
		initialiseCorners(cornerHeight);
	}
	
	private void initialiseHeightsArray()
	{
		heights = new float[resolution, resolution];
	}
		
	private void initialiseCorners(float val)
	{
		if(cornerValues == null)
			cornerValues = new float[4];
		for(int i = 0; i < cornerValues.Length; i++)
		{
			cornerValues[i] = val;
		}
	}
	
	private GridPoint findMidPoint(Quadrant quad)
	{
		GridPoint mid = new GridPoint();
		
		mid.x = Mathf.RoundToInt( ((float)(quad.tL.x + quad.tR.x)) / 2f );
		mid.z = Mathf.RoundToInt( ((float)(quad.tL.z + quad.bL.z)) / 2f );
		//mid.x = ((quad.tL.x + quad.tR.x) / 2);
		//mid.z = ((quad.tL.z + quad.bL.z) / 2);
		
		return mid;
	}
	
	private Quadrant getTopLeftQuadrant(Quadrant parent)
	{
		Quadrant quad = new Quadrant();
		
		quad.tL = parent.tL;
		quad.tR = new GridPoint(parent.mid.x, parent.tR.z);
		quad.bR = parent.mid;
		quad.bL = new GridPoint(parent.bL.x, parent.mid.z);
		
		return quad;
	}
	
	private Quadrant getTopRightQuadrant(Quadrant parent)
	{
		Quadrant quad = new Quadrant();
		
		quad.tL = new GridPoint(parent.mid.x, parent.tL.z); ;
		quad.tR = parent.tR;
		quad.bL = parent.mid;
		quad.bR = new GridPoint(parent.bR.x, parent.mid.z);
		
		return quad;
	}
	
	private Quadrant getBottomLeftQuadrant(Quadrant parent)
	{
		Quadrant quad = new Quadrant();
		
		quad.tL = new GridPoint(parent.tL.x, parent.mid.z);
		quad.tR = parent.mid;
		quad.bL = parent.bL;
		quad.bR = new GridPoint(parent.mid.x, parent.bR.z);
		
		return quad;
	}
	
	private Quadrant getBottomRightQuadrant(Quadrant parent)
	{
		Quadrant quad = new Quadrant();
		
		quad.tL = parent.mid;
		quad.tR = new GridPoint(parent.tR.x, parent.mid.z);
		quad.bL = new GridPoint(parent.mid.x, parent.bL.z);
		quad.bR = parent.bR;
		
		return quad;
	}
}
