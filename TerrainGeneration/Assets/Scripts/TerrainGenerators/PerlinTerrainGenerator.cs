﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class PerlinTerrainGenerator : MonoBehaviour {

	[HideInInspector]public PlaneMeshGenerator generator;
	[HideInInspector]public PerlinHeightMapGenerator heightMapMaker;
	[Range(0, 100)]
	public float frequency;
	[Range(0, 4)]
	public float persistance;
	[Range(1, 5)]
	public int octaves;
	[Range(0, 100)]
	public float perturbanceFrequency;
	[Range(0, 4)]
	public float perturbanceMagnitude;
	public bool perturbTerrain = false, erodeTerrain = false, livePreview = false, create = false;
	public float heightOffset;
	public int width;
	[Range(20, 254)]
	public int resolution;
	[Range(0, 2)]
	public float erosionSmoothness;
	[Range(0, 10)]
	public int erosionPasses;

	// Use this for initialization
	void Start () {
		generator = new PlaneMeshGenerator();
		heightMapMaker = new PerlinHeightMapGenerator();
		updateNoiseInfo();
		updateMapInfo();
	}
	
	private void updateNoiseInfo()
	{
		heightMapMaker.setNoiseData(frequency, persistance, octaves);
    }
    
    private void updateMapInfo()
    {
    	updateHeightMapData();
		generator.setSegments(resolution, resolution);
		generator.setDimensions(width, 0);
    }
	
	private void useGenerator()
	{
		updateNoiseInfo();
		updateMapInfo();

		generator.createMeshData();		
		createHeightMap();
		applyHeightMapToMesh();
		
		MeshFilter currFilter = GetComponent<MeshFilter>();
		DestroyImmediate(currFilter.sharedMesh); //Stops mesh leaking into memory since old one must be destroyed
		currFilter.sharedMesh = generator.createMesh();
	}
	
	private void updateHeightMapData()
	{
		heightMapMaker.setMapSize(width);
		heightMapMaker.setVertexCounts(resolution+1, resolution+1);
	}
	
	private void createHeightMap()
	{
		heightMapMaker.createHeightMap();
        if (perturbTerrain)
            heightMapMaker.perturbHeightMap(perturbanceFrequency, perturbanceMagnitude);
        if (erodeTerrain)
        {
            for (int i = 0; i < erosionPasses; ++i)
                heightMapMaker.erodeHeightMap(erosionSmoothness);
        }
	}
	
	private void applyHeightMapToMesh()
	{
		Vector3[] newVerts;
		
		newVerts = heightMapMaker.applyHeightMapData(generator.getVertexData(), heightOffset);
		
		generator.setVertexData(newVerts);
	}
	
	public void createTerrain()
	{
		create = true;
	}
	
	public void setLivePreview(bool state)
	{
		livePreview = state;
	}
	
	// Update is called once per frame
	void Update () {
		if(create)
		{
			//create stuff
			useGenerator();
			create = false;
		}
		else if(livePreview)
		{
			useGenerator();
		}
	}
}
