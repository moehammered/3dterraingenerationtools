﻿using UnityEngine;
using System.Collections;

public struct GridPoint
{
    public int x, z;

    public GridPoint(int x, int z)
    {
        this.x = x;
        this.z = z;
    }

    public override string ToString()
    {
        return "(" + x + "," + z + ")";
    }
}

public struct Quadrant
{
    public GridPoint tL, tR, bL, bR, mid;
    private GridPoint temp;
    
    public GridPoint returnTopMid()
    {
    	temp.x = mid.x;
    	temp.z = tL.z;
    	
    	return temp;
    }
    
    public GridPoint returnBotMid()
    {
		temp.x = mid.x;
		temp.z = bL.z;
		
		return temp;    	
    }
    
	public GridPoint returnLeftMid()
	{
		temp.x = tL.x;
		temp.z = mid.z;
		
		return temp;    	
	}
	
	public GridPoint returnRightMid()
	{
		temp.x = tR.x;
		temp.z = mid.z;
		
		return temp;    	
	}
}

public class MidpointDisplacementOLD : PlaneGenerator {

    public float maxHeight;
    public float[,] heights;
    public bool displayHeightPoints;
    private float currHeightOffset = 0;

    private void validateResolution()
    {
        if(resolution > 128)
        {
            resolution = 128;
            Debug.LogWarning(this + ": Resolution cannot pass 128 for a single plane");
        }

        int powerOfTwo = 1;
        while(powerOfTwo < resolution)
        {
            powerOfTwo = powerOfTwo << 1; //go to next power
        }
        resolution = powerOfTwo + 1;
    }

    private void calculateDisplacement()
    {
        heights = new float[resolution+1, resolution+1];

        Quadrant wholePlane = new Quadrant();
        wholePlane.tL = new GridPoint(0, resolution);
        wholePlane.tR = new GridPoint(resolution, resolution);
        wholePlane.bL = new GridPoint(0, 0);
        wholePlane.bR = new GridPoint(resolution, 0);
        wholePlane.mid = findMidPoint(wholePlane);

        print("TLeftMain: " + wholePlane.tL);
        print("TRightMain: " + wholePlane.tR);
        print("BLeftMain: " + wholePlane.bL);
        print("BRightMain: " + wholePlane.bR);
        print("Mid: " + wholePlane.mid);
        currHeightOffset = maxHeight;
        seedCorners(wholePlane);
        //displaceParent
        displaceQuadrant(wholePlane, 1);
    }

    private void displaceQuadrant(Quadrant quad, float level)
    {
        float tlv, trv, blv, brv, midv;
        //Get the midpoint
        quad.mid = findMidPoint(quad);
        float offset = currHeightOffset / level;
        //Get height value of corners
        tlv = heights[quad.tL.x, quad.tL.z];
        trv = heights[quad.tR.x, quad.tR.z];
        blv = heights[quad.bL.x, quad.tL.z];
        brv = heights[quad.bR.x, quad.bR.z];
        //Displace the mid point by average height of the corners
		midv = (tlv + trv + blv + brv) / 4f + Random.Range(-offset, offset);
        //Now displace the diamond points
		heights[quad.mid.x, quad.tL.z] = (tlv + trv + midv) / 3f + Random.Range(-offset, offset); //Mid along the top line
		heights[quad.mid.x, quad.bL.z] = (blv + brv + midv) / 3f + Random.Range(-offset, offset); //Mid along the bot line
        try
        {
			heights[quad.tL.x, quad.mid.z] = (tlv + blv + midv) / 3f + Random.Range(-offset, offset); //Mid along the left line
        } catch(System.IndexOutOfRangeException x)
        {
            throw;
        }
		heights[quad.tR.x, quad.mid.z] = (trv + brv + midv) / 3f + Random.Range(-offset, offset); //Mid along the right line
        //Store the mid height
        heights[quad.mid.x, quad.mid.z] = midv;
        //Segment into new quadrants
        if(quad.mid.x - quad.tL.x > 1 )
        {
            level++;
            displaceQuadrant(getTopLeftQuadrant(quad), level);
            displaceQuadrant(getTopRightQuadrant(quad), level);
            displaceQuadrant(getBottomLeftQuadrant(quad), level);
            displaceQuadrant(getBottomRightQuadrant(quad), level);
        }
    }

    private void seedCorners(Quadrant mainPlane)
    {
        heights[mainPlane.tL.x, mainPlane.tL.z] = Random.Range(0, maxHeight);
        heights[mainPlane.tR.x, mainPlane.tR.z] = Random.Range(0, maxHeight);
        heights[mainPlane.bL.x, mainPlane.bL.z] = Random.Range(0, maxHeight);
        heights[mainPlane.bR.x, mainPlane.bR.z] = Random.Range(0, maxHeight);

        heights[mainPlane.mid.x, mainPlane.mid.z] = maxHeight;
    }

    private GridPoint findMidPoint(Quadrant quad)
    {
        GridPoint mid = new GridPoint();

        mid.x = Mathf.RoundToInt( ((float)(quad.tL.x + quad.tR.x)) / 2f );
        mid.z = Mathf.RoundToInt( ((float)(quad.tL.z + quad.bL.z)) / 2f );
        //mid.x = ((quad.tL.x + quad.tR.x) / 2);
        //mid.z = ((quad.tL.z + quad.bL.z) / 2);

        return mid;
    }

    private Quadrant getTopLeftQuadrant(Quadrant parent)
    {
        Quadrant quad = new Quadrant();

        quad.tL = parent.tL;
        quad.tR = new GridPoint(parent.mid.x, parent.tR.z);
        quad.bR = parent.mid;
        quad.bL = new GridPoint(parent.bL.x, parent.mid.z);

        return quad;
    }

    private Quadrant getTopRightQuadrant(Quadrant parent)
    {
        Quadrant quad = new Quadrant();

        quad.tL = new GridPoint(parent.mid.x, parent.tL.z); ;
        quad.tR = parent.tR;
        quad.bL = parent.mid;
        quad.bR = new GridPoint(parent.bR.x, parent.mid.z);

        return quad;
    }

    private Quadrant getBottomLeftQuadrant(Quadrant parent)
    {
        Quadrant quad = new Quadrant();

        quad.tL = new GridPoint(parent.tL.x, parent.mid.z);
        quad.tR = parent.mid;
        quad.bL = parent.bL;
        quad.bR = new GridPoint(parent.mid.x, parent.bR.z);

        return quad;
    }

    private Quadrant getBottomRightQuadrant(Quadrant parent)
    {
        Quadrant quad = new Quadrant();

        quad.tL = parent.mid;
        quad.tR = new GridPoint(parent.tR.x, parent.mid.z);
        quad.bL = new GridPoint(parent.mid.x, parent.bL.z);
        quad.bR = parent.bR;

        return quad;
    }

    private void debugHeightPoints()
    {
        float offset = size / (float)resolution;
        for (int z = 0; z < heights.GetLength(1); ++z)
        {
            for (int x = 0; x < heights.GetLength(0); ++x)
            {
                Vector3 pos = new Vector3(x*offset, 0, z*offset);
                pos.y = heights[x, z];

                GameObject.CreatePrimitive(PrimitiveType.Sphere).transform.position = pos;
            }
        }
    }

    private Vector3[] convertHeightMapData(Vector3[] oldVerts, float[,] height)
    {
        Vector3[] newVerts = oldVerts;
        int i = 0;
        for (int z = 0; z < resolution; ++z, ++i)
        {
            for (int x = 0; x < resolution; ++x, ++i)
            {
                newVerts[i].y = height[x, z];
            }
            
        }
        print("Length of verts: " + newVerts.Length);
        print("Length of heights: " + heights.Length);
        return newVerts;
    }

    private void applyDisplacement()
    {
        Vector3[] verts = generator.getVertexData();
        verts = convertHeightMapData(verts, heights);
        generator.setVertexData(verts);

        DestroyImmediate(currFilter.sharedMesh); //Stops mesh leaking into memory since old one must be destroyed
        currFilter.sharedMesh = generator.createMesh();
    }

	// Use this for initialization
	void Start () {
        validateResolution();
        calculateDisplacement();
        initialiseGenerator();
        createPlane();
        applyDisplacement();
        if(displayHeightPoints)
        {
            debugHeightPoints();
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
