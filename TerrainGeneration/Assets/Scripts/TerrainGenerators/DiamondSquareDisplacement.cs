﻿using UnityEngine;
using System.Collections;

public class DiamondSquareDisplacement : PlaneGenerator {

	public DiamondSquareAlgorithm displacer;

	private void validateResolution()
	{
		if(resolution > 128)
		{
			resolution = 128;
			Debug.LogWarning(this + ": Resolution cannot pass 128 for a single plane");
		}
		
		int powerOfTwo = 1;
		while(powerOfTwo < resolution)
		{
			powerOfTwo = powerOfTwo << 1; //go to next power
		}
		resolution = powerOfTwo + 1;
	}

	private Vector3[] convertHeightMapData(Vector3[] oldVerts, float[,] height)
	{
		Vector3[] newVerts = oldVerts;
		int i = 0;
		for (int z = 0; z < resolution; ++z, ++i)
		{
			for (int x = 0; x < resolution; ++x, ++i)
			{
				newVerts[i].y = height[x, z];
			}
			
		}
		print("Length of verts: " + newVerts.Length);
		print("Length of heights: " + displacer.heights.Length);
		return newVerts;
	}
	
	private void calculateDisplacement()
	{
		displacer = new DiamondSquareAlgorithm(resolution);
		displacer.initialiseDisplacement();
	}
	
	private void applyDisplacement()
	{
		Vector3[] verts = generator.getVertexData();
		verts = convertHeightMapData(verts, displacer.heights);
		generator.setVertexData(verts);
		
		DestroyImmediate(currFilter.sharedMesh); //Stops mesh leaking into memory since old one must be destroyed
		currFilter.sharedMesh = generator.createMesh();
	}
	
	// Use this for initialization
	void Start () {
		validateResolution();
		calculateDisplacement();
		initialiseGenerator();
		createPlane();
		applyDisplacement();
		
	}
}
