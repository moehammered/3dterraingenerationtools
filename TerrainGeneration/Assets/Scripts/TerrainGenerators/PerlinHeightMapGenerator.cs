﻿using UnityEngine;
using System.Collections;
using System.IO;

[System.Serializable]
public class PerlinHeightMapGenerator
{
	public int xVertexCount, zVertexCount, octaveCount, mapWidth;
	public float frequency, persistance;
	private float[,] heights;
    private float stepWidth; //the width from point to the next, used for normalisation

	public PerlinHeightMapGenerator()
	{
		zVertexCount = xVertexCount = octaveCount = 0;
	}

	public void setNoiseData(float freq, float pers, int octaves)
	{
		persistance = pers;
		frequency = freq;
		octaveCount = octaves;
	}
	
	public void setVertexCounts(int xCount, int zCount)
	{
		zVertexCount = zCount;
		xVertexCount = xCount;
	}
	
	public void setMapSize(int size)
	{
		mapWidth = size;
	}
	
	public void createHeightMap()
	{
//		prepareTextureMap();
			
		heights = new float[xVertexCount,zVertexCount];
		generateHeightData();
	}
	
	//Currently has an issue with inconsistant results relative to width and resolution of terrain
    //[Temporarily resolved] - Was due to smoothness not being used relative to the width and resolution
    //of the terrain. Smoothness values limited to lower numbers. 
	public void erodeHeightMap(float smoothness)
	{
		for (int z = 1; z < zVertexCount - 1; z++)
		{
			for (int x = 1; x < xVertexCount - 1; x++)
			{
				float highestHeightPoint = 0.0f;
				int[] match = { 0, 0 };
				
				for (int u = -1; u <= 1; u++)
				{
					for (int v = -1; v <= 1; v++)
					{
						if(Mathf.Abs(u) + Mathf.Abs(v) > 0)
						{
							float d_i = heights[z, x] - heights[z + u, x + v];
							if (d_i > highestHeightPoint)
							{
								highestHeightPoint = d_i;
								match[0] = u; match[1] = v;
							}
						}
					}
				}

                //smoothness needs to find relativity with segments and width, for now value limiting is working
				if(0 < highestHeightPoint && highestHeightPoint <= (smoothness)) //uses smoothness which has been limited to values between 0 - 2
				{
					float averageHeight = 0.5f * highestHeightPoint;
					heights[z, x] -= averageHeight;
					heights[z + match[0], x + match[1]] += averageHeight;
				}
			}
		}
	}
	
	//Currently has an issue with inconsistant results relative to resolution of terrain - solved
	public void perturbHeightMap(float f, float d)
	{
		float[,] tempHeights;
		int u, v;
		
		tempHeights = new float[xVertexCount,zVertexCount];
		
		for(int z = 0; z < zVertexCount; ++z)
		{
			for(int x = 0; x < xVertexCount; ++x)
			{
				Vector2 point = returnNormalisedPlanePoint(x, z);

				u = x + (int)(sampleNoise(point.x, point.y, f, d, 1));
				v = z + (int)(sampleNoise(point.x, point.y, f, d, 1));
				
				u = Mathf.Clamp(u, 0, xVertexCount-1);
				v = Mathf.Clamp(v, 0, zVertexCount-1);
				
				tempHeights[x,z] = heights[v,u];
			}
		}
		
		heights = tempHeights;
	}
	
	public Vector3[] applyHeightMapData(Vector3[] oldVertexData)
	{
		Vector3[] vertexData = oldVertexData;
		int i = 0;
		for(int z = 0; z < zVertexCount; ++z)
		{
			for(int x = 0; x < xVertexCount; ++x, ++i)
			{
				vertexData[i].y = heights[x,z];
            }
        }
        return vertexData;
	}
	
	public Vector3[] applyHeightMapData(Vector3[] oldVertexData, float heightOffset)
	{
		Vector3[] vertexData = oldVertexData;
		int i = 0;
		for(int z = 0; z < zVertexCount; ++z)
		{
			for(int x = 0; x < xVertexCount; ++x, ++i)
			{
				vertexData[i].y = heights[x,z] + heightOffset;
			}
		}
		return vertexData;
	}
	
	public float[,] returnHeightsData()
	{
		return heights;
	}
	
	private void generateHeightData()
	{
        //float xPosition, zPosition;
		float fMapWidth = mapWidth;
		
		stepWidth = fMapWidth/(float)xVertexCount;
		
		for(int z = 0; z < zVertexCount; ++z)
		{
			for(int x = 0; x < xVertexCount; ++x)
			{
                //xPosition = (stepWidth * x) / fMapWidth;
                //zPosition = (stepWidth * z) / fMapWidth;
				//heights[x,z] = sampleNoise(xPosition, zPosition);
                Vector2 point = returnNormalisedPlanePoint(x, z);

                heights[x, z] = sampleNoise(point.x, point.y);
//				setTexturePixel(x, z);
			}
		}
//		saveTextureChanges();
	}
	
    private Vector2 returnNormalisedPlanePoint(float x, float z)
    {
        Vector2 normalisedPoint = Vector2.zero;
        float fMapWidth = mapWidth;

        normalisedPoint.x = (x * stepWidth) / fMapWidth;
        normalisedPoint.y = (z * stepWidth) / fMapWidth;

        return normalisedPoint;
    }

    private float sampleNoise(float x, float y, float f, float pers, int octaves)
    {
        float result = 0;
        float a = 0;
        float currF = 0;

        for (int i = 0; i <= octaves; ++i)
        {
            currF = Mathf.Pow(f, i);
            a = Mathf.Pow(pers, i);
            result += Mathf.PerlinNoise(x * currF, y * currF) * a * (mapWidth / 10f);
        }

        return result;
    }

	private float sampleNoise(float x, float y)
	{
		float result = 0;
		float a = 0;
		float f = frequency;
		
		for(int i = 0; i <= octaveCount; ++i)
		{
			f = Mathf.Pow(frequency, i);
			a = Mathf.Pow(persistance, i);
			result += Mathf.PerlinNoise(x * f, y * f) * a * (mapWidth/10f);
		}
		
		return result;
	}
}
