﻿using UnityEngine;
using System;
using System.Collections; 
using System.Collections.Generic;

public class RRTHeightMapGenerator : MonoBehaviour
{
    public RANDOM_TYPE samplingBehaviour;
    [HideInInspector]public RapidRandomTree rrt;
    public float growthRate = 4f;
    public int iterations;
    public float heightOffset;
    public MeshFilter targetMesh;
    public bool liveTerrain = false;

    private List<Vector2> points;
    private int rowCount;

    private void Awake()
    {
        initialiseRRT();
    }

    private void initialiseRRT()
    {
        rrt = new RapidRandomTree();
        rrt.setTreeProperties(iterations, growthRate, samplingBehaviour, false);
        rrt.initialise();
    }

    public void setResolution(int res)
    {
        rowCount = res + 1;
    }

    public void initialiseRRT(int iterations, float growthRate, RANDOM_TYPE type)
    {
        if(rrt != null)
        {
            rrt = null;
        }
        rrt = new RapidRandomTree();
        this.iterations = iterations;
        this.growthRate = growthRate;
        this.samplingBehaviour = type;
        rrt.setTreeProperties(this.iterations, this.growthRate, samplingBehaviour, false);
        rrt.initialise();
    }

    public void buildRRT()
    {
        rrt.updateTree();
    }

    public void findAllPointsInLines()
    {
        points = new List<Vector2>(rrt.lines.Count);
        Line[] lines = rrt.lines.ToArray();

        for (int i = 0; i < lines.Length; ++i)
        {
            //supply a line and find all points inside of it
            findPointsOnLine(lines[i]);
        }
    }

    private void findPointsOnLine(Line line)
    {
        Vector2 lineSize;
        Vector2 normalisedSize;
        Vector2 currentPointStep;
        Vector2 newPoint = line.end + line.begin;
        lineSize = line.end - line.begin;
        normalisedSize = lineSize.normalized;
        int growthRate = (int)rrt.growthRate;

        for (int i = 0; i < growthRate; ++i)
        {
            currentPointStep = normalisedSize * i; //find the point step
            newPoint = line.begin + currentPointStep; //get the new point on the line

            newPoint.x = Mathf.Round(newPoint.x); //round the values to be used for indexing
            newPoint.y = Mathf.Round(newPoint.y); //round the values to be used for indexing
            //			newPoint.x = (int)newPoint.x;
            //			newPoint.y = (int)newPoint.y;

            points.Add(newPoint);
        }
    }

    public void applyPointsToPlane()
    {
        int pointCount = points.Count;

        Vector3[] verts = targetMesh.mesh.vertices;

        for (int i = 0; i < pointCount; ++i)
        {
            int xIndex = (int)points[i].x;
            int yIndex = (int)points[i].y;
            int currentVertIndex = (rowCount) * yIndex + xIndex;

            try
            {
                verts[currentVertIndex].y = heightOffset;
                //				print ("Current vert: " + currentVertIndex + "(" + verts[currentVertIndex] + ")");
            }
            catch (Exception)
            {
                print("Out of bounds: " + currentVertIndex);
                print("RowCount: " + rowCount + " xIndex: " + xIndex + " yIndex: " + yIndex);
            }
        }

        targetMesh.mesh.vertices = verts;
        targetMesh.mesh.RecalculateNormals();
    }

    private void LateUpdate()
    {
        if(liveTerrain && rrt.finished)
        {
            applyPointsToPlane();
        }
    }
}