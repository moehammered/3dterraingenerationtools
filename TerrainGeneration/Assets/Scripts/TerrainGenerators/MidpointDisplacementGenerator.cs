﻿using UnityEngine;
using System.Collections;

namespace Incomplete
{
    public struct Quadrant
    {
        public Vector2 tLeft, tRight, bLeft, bRight;
        public Vector2 mid;
    };

    public class MidpointDisplacementGenerator : MonoBehaviour
    {

        public int width;
        public float height;
        public float heightRandomOffset;
        public float[,] heights;
        public bool checkWidth = false;
        public bool create = false;
        public bool showDebugHeights = false;

        public PlaneMeshGenerator meshGenerator;

        private float currentHeightOffset;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (checkWidth)
            {
                validateWidth();
                checkWidth = false;
            }
            if (create)
            {
                create = false;
                validateWidth();
                createHeightsArray(width + 1);
                calculateDisplacement();
                if (showDebugHeights)
                    debugHeights();

                createMeshGenerator();
                Vector3[] newVerts = applyDisplacement(meshGenerator.getVertexData());
                meshGenerator.setVertexData(newVerts);
                Mesh mesh = meshGenerator.createMesh();
                GetComponent<MeshFilter>().mesh = mesh;
            }
        }

        private void createMeshGenerator()
        {
            meshGenerator = new PlaneMeshGenerator();
            meshGenerator.setDimensions(width, (int)height);
            meshGenerator.setSegments(width, width);
            meshGenerator.createMeshData();
        }

        private Vector3[] applyDisplacement(Vector3[] oldVertexData)
        {
            Vector3[] vertexData = oldVertexData;

            int i = 0;
            for (int z = 0; z < heights.GetLength(1); ++z)
            {
                for (int x = 0; x < heights.GetLength(0); ++x, ++i)
                {
                    vertexData[i].y = heights[x, z];
                }
            }

            return vertexData;
        }

        private void debugHeights()
        {
            for (int z = 0; z < heights.GetLength(0); ++z)
            {
                for (int x = 0; x < heights.GetLength(1); ++x)
                {
                    Vector3 pos = new Vector3(x, heights[x, z], z);

                    GameObject.CreatePrimitive(PrimitiveType.Sphere).transform.position = pos;
                }
            }
        }

        private void validateWidth()
        {
            int powerOfTwo = 1;
            while (powerOfTwo < width)
            {
                powerOfTwo = powerOfTwo << 1;
            }
            width = powerOfTwo;
        }

        private void createHeightsArray(int width)
        {
            heights = new float[width, width];
            for (int z = 0; z < width; ++z)
            {
                for (int x = 0; x < width; ++x)
                {
                    heights[x, z] = height;
                }
            }
        }

        private void calculateDisplacement()
        {
            Quadrant mainSquare = new Quadrant();

            mainSquare.tLeft = new Vector2(0, 0);
            mainSquare.tRight = new Vector2(width, 0);
            mainSquare.bLeft = new Vector2(0, width);
            mainSquare.bRight = new Vector2(width, width);

            print("TLeftMain: " + mainSquare.tLeft);
            print("TRightMain: " + mainSquare.tRight);
            print("BLeftMain: " + mainSquare.bLeft);
            print("BRightMain: " + mainSquare.bRight);

            currentHeightOffset = heightRandomOffset;
            displaceParentPoints(mainSquare);
        }

        private void displaceParentPoints(Quadrant parent)
        {
            Vector2 mid = findMidPoint(parent);
            parent.mid = mid;

            float topLeftHeight = heights[(int)parent.tLeft.x, (int)parent.tLeft.y] + Random.Range(-heightRandomOffset, heightRandomOffset); //heights point from tLeft
            float topRightHeight = heights[(int)parent.tRight.x, (int)parent.tRight.y] + Random.Range(-heightRandomOffset, heightRandomOffset);
            float bottomLeftHeight = heights[(int)parent.bLeft.x, (int)parent.bLeft.y] + Random.Range(-heightRandomOffset, heightRandomOffset);
            float bottomRightHeight = heights[(int)parent.bRight.x, (int)parent.bRight.y] + Random.Range(-heightRandomOffset, heightRandomOffset);
            //displace heights array at midpoint
            float midHeightAvg = calculateAverageMidpointHeight(topLeftHeight, topRightHeight, bottomLeftHeight, bottomRightHeight);
            midHeightAvg += Random.Range(-currentHeightOffset, currentHeightOffset);
            heights[(int)mid.x, (int)mid.y] = midHeightAvg;

            currentHeightOffset /= 2;
            displaceDiamondPoints(parent, calculateAverageDiamondHeight(topLeftHeight, topRightHeight, bottomLeftHeight, bottomRightHeight));

            Quadrant topLeft, topRight, bottomLeft, bottomRight;

            topLeft = getTopLeftQuadrant(parent);
            topRight = getTopRightQuadrant(parent);
            bottomLeft = getBottomLeftQuadrant(parent);
            bottomRight = getBottomRightQuadrant(parent);

            if ((int)mid.x - (int)parent.tLeft.x > 1)
            {
                //split quadrants, calculate mid points for new quadrants
                displaceParentPoints(topLeft);
                displaceParentPoints(topRight);
                displaceParentPoints(bottomLeft);
                displaceParentPoints(bottomRight);
            }
        }

        private Quadrant getTopLeftQuadrant(Quadrant quadrant)
        {
            Quadrant newQuadrant = new Quadrant();

            newQuadrant.tLeft = quadrant.tLeft;
            newQuadrant.tRight = new Vector2(quadrant.mid.x, quadrant.tRight.y);
            newQuadrant.bLeft = new Vector2(quadrant.bLeft.x, quadrant.mid.y);
            newQuadrant.bRight = quadrant.mid;

            return newQuadrant;
        }

        private Quadrant getTopRightQuadrant(Quadrant quadrant)
        {
            Quadrant newQuadrant = new Quadrant();

            newQuadrant.tLeft = new Vector2(quadrant.mid.x, quadrant.tLeft.y);
            newQuadrant.tRight = quadrant.tRight;
            newQuadrant.bLeft = quadrant.mid;
            newQuadrant.bRight = new Vector2(quadrant.bRight.x, quadrant.mid.y);

            return newQuadrant;
        }

        private Quadrant getBottomLeftQuadrant(Quadrant quadrant)
        {
            Quadrant newQuadrant = new Quadrant();

            newQuadrant.tLeft = new Vector2(quadrant.tLeft.x, quadrant.mid.y);
            newQuadrant.tRight = quadrant.mid;
            newQuadrant.bLeft = quadrant.bLeft;
            newQuadrant.bRight = new Vector2(quadrant.mid.x, quadrant.bRight.y);

            return newQuadrant;
        }

        private Quadrant getBottomRightQuadrant(Quadrant quadrant)
        {
            Quadrant newQuadrant = new Quadrant();

            newQuadrant.tLeft = quadrant.mid;
            newQuadrant.tRight = new Vector2(quadrant.tRight.x, quadrant.mid.y);
            newQuadrant.bLeft = new Vector2(quadrant.mid.x, quadrant.bRight.y);
            newQuadrant.bRight = quadrant.bRight;

            return newQuadrant;
        }

        private float calculateAverageMidpointHeight(float tLeft, float tRight, float bLeft, float bRight)
        {
            float avgHeight = 0;

            avgHeight = (tLeft + tRight + bLeft + bRight) / 4.0f; //remember to add a random height amount

            return avgHeight;
        }

        private float[] calculateAverageDiamondHeight(float tLeft, float tRight, float bLeft, float bRight)
        {
            float[] points = new float[4];

            points[0] = (tLeft + bLeft) / 2f + Random.Range(0, heightRandomOffset); //Mid-Left point
            points[1] = (tLeft + tRight) / 2f + Random.Range(0, heightRandomOffset); //Mid-Top point
            points[2] = (tRight + bRight) / 2f + Random.Range(0, heightRandomOffset); //Mid-Right point
            points[3] = (bRight + bLeft) / 2f + Random.Range(0, heightRandomOffset); //Mid-Bottom point

            return points;
        }

        private void displaceDiamondPoints(Quadrant currentQuadrant, float[] pointValues)
        {
            displaceHeightPoint(currentQuadrant.tLeft.x, currentQuadrant.mid.y, pointValues[0]); //Left mid point
            displaceHeightPoint(currentQuadrant.mid.x, currentQuadrant.tLeft.y, pointValues[1]); //Top mid point
            displaceHeightPoint(currentQuadrant.tRight.x, currentQuadrant.mid.y, pointValues[2]); //right mid point
            displaceHeightPoint(currentQuadrant.mid.x, currentQuadrant.bRight.y, pointValues[3]); //bottom mid point
        }

        private Vector2 findMidPoint(Quadrant quadrant)
        {
            Vector2 midPoint = Vector2.zero;

            int x, z;

            x = (int)quadrant.tLeft.x + (int)quadrant.tRight.x;
            z = (int)quadrant.tRight.y + (int)quadrant.bRight.y;

            x /= 2;
            z /= 2;

            midPoint.x = x;
            midPoint.y = z;

            return midPoint;
        }

        private void displaceHeightPoint(float x, float y, float value)
        {
            heights[(int)x, (int)y] = value;
        }
    }
}