﻿using UnityEngine;
using System.Collections;
using System.IO;

[ExecuteInEditMode]

public class HeightMapToTexture : MonoBehaviour {

	public int textureWidth, textureHeight;
	public float peakHeight, valleyHeight, groundLevel, seaHeight, maximumHeight;
	public Color peaks, valleys, ground, sea;
	public bool generateTexture, greyScaleMap;
	
	private float[,] heights;
	private Texture2D textureMap, greyScaleTexture;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(generateTexture)
		{
			DestroyImmediate(textureMap);
            DestroyImmediate(greyScaleTexture);
			generateTexture = false;
			getHeightData();
			calculateColours();
			finaliseTexture();
			giveRendererTexture();
		}
	}
	
    private void getMaximumHeight()
    {
        int xExtent = heights.GetLength(0);
        int yExtent = heights.GetLength(1);

        for (int y = 0; y < yExtent; ++y)
        {
            for (int x = 0; x < xExtent; ++x)
            {
                if (maximumHeight < heights[x, y])
                    maximumHeight = heights[x, y];
            }
        }
    }

	private void getHeightData()
	{
		PerlinTerrainGenerator tg = GetComponent<PerlinTerrainGenerator>();
		PerlinHeightMapGenerator hm;
		
		if(tg != null)
		{
			hm = tg.heightMapMaker;
            if (hm != null)
            {
                heights = hm.returnHeightsData();
                if(greyScaleMap)
                    getMaximumHeight();
            }
		}
		
		if(heights != null)
		{
			prepareTexture();
		}
	}
	
	private void calculateColours()
	{
		try
		{
			int xExtent = heights.GetLength(0);
			int yExtent = heights.GetLength(1);
			
			for(int y = 0; y < yExtent; ++y)
			{
				for(int x = 0; x < xExtent; ++x)
				{
					textureMap.SetPixel(x, y, createColour(x, y));
                    if (greyScaleMap)
                        greyScaleTexture.SetPixel(x, y, createGrey(x, y));
				}
			}
		} catch(System.Exception e)
		{
			print (e.Message);
		}
	}
	
	private void finaliseTexture()
	{
		try
		{
			textureMap.Apply();
			#if (UNITY_WEBPLAYER != true && UNITY_WEBGL != true)
			saveTexture(textureMap, Application.dataPath + "/" + textureMap.name);
            if(greyScaleMap)
                saveTexture(greyScaleTexture, Application.dataPath + "/" + greyScaleTexture.name);
			#endif
		} catch(System.Exception e)
		{
			print (e.Message);
		}
	}
	
	private void giveRendererTexture()
	{
		GetComponent<Renderer>().material.mainTexture = textureMap;
	}
	
	private Color createColour(int x, int y)
	{
		Color heightCol;
		float currHeight = heights[x, y];
		
		if(currHeight > peakHeight)
		{
			heightCol = peaks;
		}
		else if(currHeight > valleyHeight)
		{
			heightCol = valleys;
		}
		else if(currHeight > groundLevel)
		{
			heightCol = ground;
		}
		else
		{
			heightCol = sea;
		}
		
		return heightCol;
	}

    private Color createGrey(int x, int y)
    {
        Color heightCol;
        float currHeight = heights[x, y];

        currHeight /= maximumHeight;
        heightCol = new Color(currHeight, currHeight, currHeight);

        return heightCol;
    }
	
	private void prepareTexture()
	{
		if(textureMap == null)
		{
			textureMap = new Texture2D(heights.GetLength(0), heights.GetLength(1), TextureFormat.ARGB32, false);
			textureMap.name = "HeightToTexture";
		}
		else
			textureMap.Resize(heights.GetLength(0), heights.GetLength(1));
        if (greyScaleTexture == null)
        {
            greyScaleTexture = new Texture2D(heights.GetLength(0), heights.GetLength(1), TextureFormat.ARGB32, false);
            greyScaleTexture.name = "HeightToGrey";
        }
        else
            greyScaleTexture.Resize(heights.GetLength(0), heights.GetLength(1));
	}
	
	public Texture2D getHeightTexture(bool getGreyScale)
    {
        if (getGreyScale)
            return greyScaleTexture;
        return textureMap;
    }
	/**
	 * Saves a texture given to it into a filepath.
	 */
	private void saveTexture(Texture2D newTexture, string filePath)
	{
		#if (UNITY_WEBPLAYER != true && UNITY_WEBGL != true)
		byte[] bytes = newTexture.EncodeToPNG();
		
		File.WriteAllBytes(filePath + ".png", bytes);
		#endif
		#if UNITY_EDITOR
		UnityEditor.AssetDatabase.Refresh();
		#endif
	}
	
}
