﻿using UnityEngine;
using System.Collections;

public class PerlinTerrainController : MonoBehaviour {

	public PerlinTerrainGenerator generator;
	public HeightMapToTexture textureMaker;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void createTexture()
	{
		textureMaker.generateTexture = true;
	}
	
    public void setGreyscale(bool value)
    {
        textureMaker.greyScaleMap = value;
    }

	public void setPerturb(bool value)
	{
		generator.perturbTerrain = value;
	}
	
	public void setErosion(bool value)
	{
		generator.erodeTerrain = value;
	}
	
	public void setErosionPasses(float value)
	{
		generator.erosionPasses = (int)value;
	}
	
	public void setErosionSmoothness(float value)
	{
		generator.erosionSmoothness = value;
	}
	
	public void setPerturbMagnitude(float value)
	{
		generator.perturbanceMagnitude = value;
	}
	
	public void setPerturbFrequency(float value)
	{
		generator.perturbanceFrequency = value;
	}
	
	public void setResolution(float value)
	{
		generator.resolution = (int)value;
	}
	
	public void setFrequency(float value)
	{
		generator.frequency = value;
	}
	
	public void setPersistance(float value)
	{
		generator.persistance = value;
	}
	
	public void setOctaves(float value)
	{
		generator.octaves = (int)value;
	}
	
	public void generateTerrain()
	{
		generator.createTerrain();
	}

    public void saveMesh(string filename)
    {
        MeshFilter filter = generator.GetComponent<MeshFilter>();
        Renderer rend = generator.GetComponent<Renderer>();
#if UNITY_EDITOR
        //figure out how to save from webgl
        string path = UnityEditor.EditorUtility.OpenFolderPanel("Choose Save Location", Application.dataPath, "");
#else
        string path = Application.dataPath;
#endif
        ObjExporter.MeshToFile(filter, path + "/" + filename + ".obj");
        saveTexture((Texture2D)rend.material.mainTexture, path + "/");
        if (textureMaker.greyScaleMap)
            saveTexture(textureMaker.getHeightTexture(true), path + "/");
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
    }
	
	public void setLiveTerrain(bool state)
	{
		generator.setLivePreview(state);
	}

    /**
     * Saves a texture given to it into a filepath.
     */
    private void saveTexture(Texture2D newTexture, string filePath)
    {
        #if (UNITY_WEBPLAYER != true && UNITY_WEBGL != true)
        byte[] bytes = newTexture.EncodeToPNG();
        filePath += newTexture.name;
        System.IO.File.WriteAllBytes(filePath + ".png", bytes);
        #endif
        #if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
        #endif
    }
}
