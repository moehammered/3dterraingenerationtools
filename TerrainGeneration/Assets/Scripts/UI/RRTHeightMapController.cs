﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RRTHeightMapController : MonoBehaviour {

    public RRTHeightMapGenerator rrtMap;
    public Text samplingTextField, growthTextField;
    private int iterations = 20;
    private float growthRate;
    private RANDOM_TYPE samplingBehaviour;
    
	// Use this for initialization
	void Start () {
        setGrowth(1);
        setSamplingMethod(0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setResolution(float res)
    {
        iterations = (int)res;
        rrtMap.setResolution(iterations);
    }

    public void setGrowth(float value)
    {
        growthRate = value;
        growthTextField.text = "Value: " + value;
    }

    public void setSamplingMethod(float value)
    {
        samplingBehaviour = (RANDOM_TYPE)value;

        switch(samplingBehaviour)
        {
            case RANDOM_TYPE.UNIQUE_POINTS:
                samplingTextField.text = "Method: UNIQUE";
                break;

            case RANDOM_TYPE.CHAOTIC_POINTS:
                samplingTextField.text = "Method: CHAOTIC";
                break;
        }
    }

    public void buildRRT()
    {
        rrtMap.initialiseRRT(iterations, growthRate, samplingBehaviour);
        rrtMap.buildRRT();
    }

    public void applyRRT()
    {
        rrtMap.findAllPointsInLines();
        rrtMap.applyPointsToPlane();
    }

    public void setLiveMode(bool value)
    {
        rrtMap.liveTerrain = value;
    }
}
