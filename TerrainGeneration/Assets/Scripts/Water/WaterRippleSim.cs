﻿using UnityEngine;
using System.Collections;

public class WaterRippleSim : PlaneGenerator {

    public GameObject sphereMarker;
    public float[,] buffer1, buffer2;
    public int rows, columns;
    [Range(0, 1)]
    public float damping;
    public int x, z, force;
    public bool applyForce, snapshot, debugMode;
    public Vector3[] vertSnap;
    private bool toggleBuffers;
    private MeshFilter currFilter;

    private void initialiseBuffers()
    {
        rows = generator.verticalSegments;
        columns = generator.horizontalSegments;

        buffer1 = new float[columns, rows];
        buffer2 = new float[columns, rows];
    }

    private float[,] processBuffers(float[,] previousFrame, float[,] currentFrame)
    {
        float[,] newBuffer = new float[columns, rows];

        float leftPoint, rightPoint, topPoint, botPoint;
        float averagedPoint = 0;

        for (int z = 1; z < rows-1; ++z)
        {
            for (int x = 1; x < columns-1; ++x )
            {
                leftPoint = previousFrame[x - 1, z];
                rightPoint = previousFrame[x + 1, z];
                topPoint = previousFrame[x, z + 1];
                botPoint = previousFrame[x, z - 1];
                averagedPoint = (leftPoint + rightPoint + topPoint + botPoint) / 4f - currentFrame[x,z];

                averagedPoint *= damping;

                newBuffer[x, z] = averagedPoint;
            }
        }

        return newBuffer;
    }

    private Vector3[] applyHeightMapData(Vector3[] oldVerts, float[,] waterMap)
    {
        Vector3[] newVerts = oldVerts;
        int i = 0;
        for (int z = 0; z < rows; ++z, ++i)
        {
            for (int x = 0; x < columns; ++x, ++i)
            {
                newVerts[i].y = waterMap[x, z];
                if(debugMode && this.x == x && this.z == z)
                {
                    sphereMarker.transform.position = newVerts[i];
                }
            }
        }

        return newVerts;
    }

    private void splash(int x, int z)
    {
        if (x > columns - 2)
            x = columns - 2;
        else if (x < 1)
            x = 1;

        if (z > rows - 2)
            z = rows - 2;
        else if (z < 1)
            z = 1;

        buffer1[x, z] = force; //Centre point

        buffer1[x - 1, z] = force; //left point
        buffer1[x + 1, z] = force; //right point
        buffer1[x, z + 1] = force; //top point
        buffer1[x, z - 1] = force; //bot point

        buffer1[x - 1, z + 1] = force; //top left point
        buffer1[x + 1, z + 1] = force; //top right point
        buffer1[x - 1, z - 1] = force; //bot left point
        buffer1[x + 1, z - 1] = force; //bot right point
    }

    private void applyWater(float[,] waterMap)
    {
        Vector3[] verts = generator.getVertexData();
        verts = applyHeightMapData(verts, waterMap);
        if(debugMode && snapshot)
            vertSnap = verts;
        snapshot = false;
        generator.setVertexData(verts);

        DestroyImmediate(currFilter.sharedMesh); //Stops mesh leaking into memory since old one must be destroyed
        currFilter.sharedMesh = generator.createMesh();
    }

    // Use this for initialization
    void Start()
    {
        currFilter = GetComponent<MeshFilter>();
        sphereMarker = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        if (!debugMode)
            sphereMarker.SetActive(false);
        initialiseBuffers();
    }

	// Update is called once per frame
	void Update () {
        float[,] newBuffer = null;
        if(applyForce)
        {
            snapshot = true;
            applyForce = false;
            splash(x, z);
        }
        if(toggleBuffers)
        {
            newBuffer = processBuffers(buffer2, buffer1);
            buffer1 = newBuffer;
        }
        else
        {
            newBuffer = processBuffers(buffer1, buffer2);
            buffer2 = newBuffer;
        }
        applyWater(newBuffer);

        toggleBuffers = !toggleBuffers;
	}
}
