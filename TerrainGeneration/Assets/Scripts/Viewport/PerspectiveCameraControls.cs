﻿using UnityEngine;
using System.Collections;

public class PerspectiveCameraControls : MonoBehaviour {

	public float cameraRotationSensitivity = 1f, cameraPanSensitivity = 1f, cameraZoomSensitivity = 1f;
	public KeyCode modifierKey = KeyCode.BackQuote;
	public KeyCode resetKey = KeyCode.RightShift;
	
	public bool useModifier = false;
	public Transform rotateTarget;
	
	private Vector3 defaultPosition;
	private Vector3 mouseRotDelta, mouseMoveDelta, rotationEulerAngles;
	private Quaternion defaultRotation;
	
    public void setRotationSensitivity(float value)
    {
        cameraRotationSensitivity = value;
    }

    public void setPanSensitivity(float value)
    {
        cameraPanSensitivity = value;
    }

    public void setZoomSensitivity(float value)
    {
        cameraZoomSensitivity = value;
    }

	private void setDefaults()
	{
		mouseRotDelta = Vector3.zero;
		mouseMoveDelta = Vector3.zero;
		defaultPosition = transform.position;
		defaultRotation = transform.rotation;
		rotationEulerAngles = defaultRotation.eulerAngles;
	}
	
	// Use this for initialization
	void Start () {
		setDefaults();
	}
	
	// Update is called once per frame
	void Update () {
		if(!useModifier)
		{
			checkNormalKeybindings();
		}
		else
		{
			checkModifiedBindings();
		}
		checkModifierKey();
	}
	
	private void LateUpdate()
	{
		zoomCamera(Input.mouseScrollDelta.y * cameraZoomSensitivity);
	}
	
	private void checkModifierKey()
	{
		if(Input.GetKeyDown(modifierKey))
		{
			toggleModifier();
		}
	}
	
	private void checkNormalKeybindings()
	{
		getMouseDeltas();
		if(Input.GetKeyDown(resetKey))
		{
			resetDefaultTransform();
		}
		if(Input.GetMouseButton(1))
		{
			rotateCamera();
		}
		else if(Input.GetMouseButton(2))
		{
			panCamera();
		}
	}
	
	private void checkModifiedBindings()
	{
		
	}
	
	private void resetDefaultTransform()
	{
		transform.position = defaultPosition;
		transform.rotation = defaultRotation;
        setDefaults();
	}
	
	private void panCamera()
	{
		transform.Translate(mouseMoveDelta * cameraPanSensitivity, Space.Self); //Don't need to use deltaTime because mouseMoveDelta is frame-rate independent
	}
	
	private void getMouseDeltas()
	{
		mouseRotDelta.y = Input.GetAxis("Mouse X") * cameraRotationSensitivity;
		mouseRotDelta.x = -Input.GetAxis("Mouse Y") * cameraRotationSensitivity;
		
		mouseMoveDelta.x = -Input.GetAxis("Mouse X");
		mouseMoveDelta.y = -Input.GetAxis("Mouse Y");
	}
	
	private void rotateCamera()
	{
//		transform.RotateAround(rotateTarget.transform.position, rotationAxis, 1);
//		rotationAxis = Vector3.zero;
//		transform.Rotate(mouseRotDelta.x, 0, 0, Space.World);
//		transform.Rotate(0, mouseRotDelta.y, 0, Space.Self);
		rotationEulerAngles.x += mouseRotDelta.x;
		rotationEulerAngles.y += mouseRotDelta.y;
		
		transform.rotation = Quaternion.Euler(rotationEulerAngles); //avoids gimbal lock
	}
	
	private void toggleModifier()
	{
		useModifier = !useModifier;
	}
	
	private void zoomCamera(float value)
	{
		transform.Translate(transform.forward * value, Space.World);
	}
}
