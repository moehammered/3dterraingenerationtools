﻿using UnityEngine;
using System.Collections;

public class ViewportSwitcher : MonoBehaviour {

	public Camera[] cameras;
	public Camera currentCam;
	
	// Use this for initialization
	void Start () {
		currentCam = cameras[0];
	}
	
	// Update is called once per frame
	void Update () {
		checkLeftClick();
	}
	
	private void checkLeftClick()
	{
		if(Input.GetMouseButtonDown(0))
		{
			Camera view = detectViewportCamera();
			if(view != null && view != currentCam)
				switchViewports(currentCam, view);
		}
	}
	
	private Camera detectViewportCamera()
	{
		Camera viewport = null;

		for(int i = 0; i < cameras.Length; ++i)
		{
			if(cameras[i].pixelRect.Contains(Input.mousePosition))
			{
				viewport = cameras[i];
				break;
			}
		}
		
		return viewport;
	}
	
	private void switchViewports(Camera currView, Camera newView)
	{
		toggleCameraControls(currView, newView);
		Rect currViewport, newViewport;
		currViewport = currView.rect;
		newViewport = newView.rect;
		
		currentCam = newView;
		currentCam.rect = currViewport;
		newView = currView;
		newView.rect = newViewport;
	}
	
	private void toggleCameraControls(Camera currView, Camera newView)
	{
		PerspectiveCameraControls oldCam, newCam;
		
		oldCam = currView.gameObject.GetComponent<PerspectiveCameraControls>();
		newCam = newView.gameObject.GetComponent<PerspectiveCameraControls>();
		
		oldCam.enabled = false;
		newCam.enabled = true;
	}
}
